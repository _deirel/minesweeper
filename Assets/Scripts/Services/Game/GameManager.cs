﻿using Logic.Game;
using UnityEngine.SceneManagement;

namespace Services.Game
{
    public class GameManager : IGameManager
    {
        private IGameModelInitializer _gameModelInitializer;

        public void NewGame(GameMode mode)
        {
            StartGameWith(new GameModelByModeInitializer(mode));
        }

        public void LoadGame()
        {
            StartGameWith(new GameModelFromSaveInitializer());
        }

        private void StartGameWith(IGameModelInitializer initializer)
        {
            _gameModelInitializer = initializer;
            _gameModelInitializer.Prepare();
            SceneManager.LoadScene("Game");
        }

        public void ExitGame()
        {
            SceneManager.LoadScene("Menu");
        }

        public IGameModelInitializer GetGameModelInitializer()
        {
            return _gameModelInitializer;
        }
    }
}