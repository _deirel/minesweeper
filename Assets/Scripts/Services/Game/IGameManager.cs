﻿using Logic.Game;

namespace Services.Game
{
    public interface IGameManager
    {
        void NewGame(GameMode mode);
        void LoadGame();
        void ExitGame();
        IGameModelInitializer GetGameModelInitializer();
    }
}