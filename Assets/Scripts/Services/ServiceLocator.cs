﻿using Services.Game;
using Services.Save;

namespace Services
{
    public class ServiceLocator : IServiceLocator
    {
        private static IServiceLocator _instance;

        public static IServiceLocator instance
        {
            get { return _instance ?? (_instance = new ServiceLocator()); }
        }

        public ISaveSystem saveSystem { get; private set; }
        public IGameManager gameManager { get; private set; }

        private ServiceLocator()
        {
            saveSystem = new JsonSaveSystem();
            gameManager = new GameManager();
        }
    }
}