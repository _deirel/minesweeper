﻿using Logic.Save;

namespace Services.Save
{
    public interface ISaveSystem
    {
        SaveData LoadData();
        void SaveData(SaveData data);
        bool IsSaveExisted();
    }
}