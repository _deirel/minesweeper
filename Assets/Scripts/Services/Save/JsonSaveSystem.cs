﻿using System;
using System.IO;
using Logic.Save;
using UnityEngine;

namespace Services.Save
{
    public class JsonSaveSystem : ISaveSystem
    {
        private readonly string _savePath;

        public JsonSaveSystem()
        {
            _savePath = Application.persistentDataPath + "/save.json";
        }
        
        public SaveData LoadData()
        {
            try
            {
                var rawData = File.ReadAllText(_savePath);
                var saveData = JsonUtility.FromJson<SaveData>(rawData);
                if (!saveData.IsValid())
                {
                    throw new InvalidSaveException();
                }

                return saveData;
            }
            catch (InvalidSaveException)
            {
                throw;
            }
            catch (Exception)
            {
                throw new InvalidSaveException();
            }
        }

        public void SaveData(SaveData data)
        {
            var rawData = JsonUtility.ToJson(data);
            File.WriteAllText(_savePath, rawData);
        }

        public bool IsSaveExisted()
        {
            return File.Exists(_savePath);
        }
    }
}