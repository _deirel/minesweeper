﻿using System;

namespace Services.Save
{
    public class InvalidSaveException : Exception
    {
        public InvalidSaveException() : base("Invalid save format")
        {
        }
    }
}