﻿using Services.Game;
using Services.Save;

namespace Services
{
    public interface IServiceLocator
    {
        ISaveSystem saveSystem { get; }
        IGameManager gameManager { get; }
    }
}