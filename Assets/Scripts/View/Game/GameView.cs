﻿using System.Collections.Generic;
using Logic.Game;
using UnityEngine;
using UnityEngine.Events;
using View.Game.Highlight;

namespace View.Game
{
    [RequireComponent(typeof(GameFieldPointerHandler))]
    [RequireComponent(typeof(GameFieldView))]
    [RequireComponent(typeof(GameFieldHighlighter))]
    public class GameView : MonoBehaviour
    {
        public GameModel gameModel;

        private GameFieldView _field;
        private GameFieldHighlighter _highlighter;

        [HideInInspector] public UnityEvent onFieldCreated = new UnityEvent();

        private void Start()
        {
            _field = GetComponent<GameFieldView>();
            _highlighter = GetComponent<GameFieldHighlighter>();
            AddModelListeners();
        }

        private void AddModelListeners()
        {
            gameModel.onCellsGenerated.AddListener(ModelCellsGeneratedHandler);
            gameModel.onCellOpen.AddListener(ModelCellOpenHandler);
            gameModel.onCellSetMark.AddListener(ModelCellSetMarkHandler);
        }

        public void CreateField()
        {
            _field.AutoSizeRectTransform();
            _field.CreateCells();
            onFieldCreated.Invoke();
        }

        private void ModelCellsGeneratedHandler()
        {
            _field.SetCellsByModel();
        }

        private void ModelCellOpenHandler(int x, int y)
        {
            _field.GetCell(x, y).Open(true);
        }

        private void ModelCellSetMarkHandler(int x, int y, bool markSet)
        {
            _field.GetCell(x, y).SetMark(markSet);
        }

        public void SetHighlightion(List<Vector2Int> positions)
        {
            _highlighter.SetHighlightion(positions);
        }
        
        public int numCellsX
        {
            get { return gameModel.width; }
        }
        
        public int numCellsY
        {
            get { return gameModel.height; }
        }

        private void OnDestroy()
        {
            onFieldCreated.RemoveAllListeners();
        }
    }
}