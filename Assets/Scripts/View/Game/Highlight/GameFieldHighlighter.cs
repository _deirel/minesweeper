﻿using System.Collections.Generic;
using UnityEngine;

namespace View.Game.Highlight
{
    public class GameFieldHighlighter : MonoBehaviour
    {
        private GameFieldView _fieldView;

        private readonly Dictionary<CellView, bool> _selectedCells = new Dictionary<CellView, bool>();

        private void Start()
        {
            _fieldView = GetComponent<GameFieldView>();
        }

        public void SetHighlightion(List<Vector2Int> positions)
        {
            DeselectAllExcept(positions);
            if (positions == null)
            {
                return;
            }

            foreach (var position in positions)
            {
                var cell = _fieldView.GetCell(position.x, position.y);
                if (cell != null && !_selectedCells.ContainsKey(cell))
                {
                    cell.Highlight();
                    _selectedCells[cell] = true;
                }
            }
        }

        private void DeselectAllExcept(List<Vector2Int> except)
        {
            List<CellView> cellsToRemoveFromDictionary = new List<CellView>();

            foreach (var pair in _selectedCells)
            {
                var cell = pair.Key;
                if (!except.Exists(p => p.x == cell.x && p.y == cell.y))
                {
                    cell.Unhighlight();
                    cellsToRemoveFromDictionary.Add(cell);
                }
            }

            foreach (var cell in cellsToRemoveFromDictionary)
            {
                _selectedCells.Remove(cell);
            }
        }
    }
}