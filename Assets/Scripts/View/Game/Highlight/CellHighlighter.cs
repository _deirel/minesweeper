﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.Game.Highlight
{
    public class CellHighlighter : MonoBehaviour
    {
        public RawImage[] imagesToTint;
        public Color color;

        private Dictionary<RawImage, Color> _initialColors = new Dictionary<RawImage, Color>();

        private void Start()
        {
            foreach (var image in imagesToTint)
            {
                _initialColors[image] = image.color;
            }
        }

        public void Hightlight()
        {
            foreach (var image in imagesToTint)
            {
                image.color = color;
            }
        }

        public void Unhighlight()
        {
            foreach (var image in imagesToTint)
            {
                image.color = _initialColors[image];
            }
        }
    }
}