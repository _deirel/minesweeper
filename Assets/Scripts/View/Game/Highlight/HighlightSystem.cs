﻿using System.Collections.Generic;
using UnityEngine;
using Utils;
using View.Game.Control;

namespace View.Game.Highlight
{
    public class HighlightSystem : MonoBehaviour
    {
        public GameView gameView;
        public ControlSystem controls;

        private bool _bothButtonsMode;
        private Vector2Int? _currentCell;
        
        private void Start()
        {
            controls.onEnterBothButtonsMode.AddListener(EnterBothButtonsModeHandler);
            controls.onExitBothButtonsMode.AddListener(ExitBothButtonsModeHandler);
            controls.onChangeCellUnderPointer.AddListener(ChangeCellUnderPointerHandler);
        }

        private void EnterBothButtonsModeHandler()
        {
            _bothButtonsMode = true;
            UpdateSelection();
        }

        private void ExitBothButtonsModeHandler()
        {
            _bothButtonsMode = false;
            UpdateSelection();
        }

        private void ChangeCellUnderPointerHandler(Vector2Int? position)
        {
            _currentCell = position;
            UpdateSelection();
        }

        private void UpdateSelection()
        {
            var positions = new List<Vector2Int>();

            if (_currentCell != null)
            {
                var value = _currentCell.Value;
                positions.Add(value);
                if (_bothButtonsMode)
                {
                    CoordsUtil.InvokeForNearestPositions(value.x, value.y, gameView.numCellsX, gameView.numCellsY,
                        (x, y) =>
                        {
                            positions.Add(new Vector2Int(x, y));
                        });
                }
            }
            
            gameView.SetHighlightion(positions);
        }
    }
}