﻿using UnityEngine;
using UnityEngine.Events;

namespace View.Game.Events
{
    public class CellEvent : UnityEvent<Vector2Int?>
    {
    }
}