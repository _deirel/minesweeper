﻿using Logic.Game;
using UnityEngine.Events;

namespace View.Game.Events
{
    public class CellClickEvent : UnityEvent<int, int, ClickType>
    {
    }
}