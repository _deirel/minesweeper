﻿using System;
using Logic.Game;
using UnityEngine;
using Utils;

namespace View.Game
{
    public class GameFieldView : MonoBehaviour
    {
        private const float CELL_SIZE = 64.0f;
        private const float CELL_INDENT = 4.0f;

        public GameObject cellPrefab;
        public GameModel gameModel;
        
        private CellView[,] _cells;

        public void AutoSizeRectTransform()
        {
            var fieldWidth = gameModel.width * CELL_SIZE + (gameModel.width - 1) * CELL_INDENT;
            var fieldHeight = gameModel.height * CELL_SIZE + (gameModel.height - 1) * CELL_INDENT;
            GetComponent<RectTransform>().sizeDelta = new Vector2(fieldWidth, fieldHeight);
        }

        public void CreateCells()
        {
            _cells = new CellView[gameModel.width, gameModel.height];
            
            var x0 = -(gameModel.width - 1) * (CELL_SIZE + CELL_INDENT) / 2;
            var y0 = -(gameModel.height - 1) * (CELL_SIZE + CELL_INDENT) / 2;
            for (var i = 0; i < gameModel.width; i++)
            {
                for (var j = 0; j < gameModel.height; j++)
                {
                    var cellGameObject = Instantiate(cellPrefab, transform);
                    cellGameObject.transform.localPosition = new Vector3(
                        x0 + i * (CELL_SIZE + CELL_INDENT),
                        y0 + j * (CELL_SIZE + CELL_INDENT),
                        0f
                    );

                    var cellView = cellGameObject.GetComponent<CellView>();
                    InitCellView(cellView, i, j);
                }
            }
        }

        private void InitCellView(CellView view, int x, int y)
        {
            view.x = x;
            view.y = y;
//            view.onClick.AddListener(CellClickHandler);
            _cells[x, y] = view;
        }

        public void SetCellsByModel()
        {
            foreach (var cellView in _cells)
            {
                var cellModel = gameModel.GetCell(cellView.x, cellView.y);
                SetCellViewByModel(cellView, cellModel);
            }
        }

        private static void SetCellViewByModel(CellView view, Cell model)
        {
            if (model.isBomb)
            {
                view.SetAsBomb();
            }
            else if (model.numBombsNear == 0)
            {
                view.SetAsEmpty();
            }
            else
            {
                view.SetAsHasBombNear(model.numBombsNear);
            }

            if (model.isOpened)
            {
                view.Open(false);
            }
            else if (model.isMarked)
            {
                view.SetMark(true);
            }
        }

        public CellView GetCell(int x, int y)
        {
            return CoordsUtil.IsInRange(x, y, gameModel.width, gameModel.height) ? _cells[x, y] : null;
        }

        public void ForEachCell(Action<CellView> callback)
        {
            foreach (var cellView in _cells)
            {
                callback(cellView);
            }
        }
    }
}