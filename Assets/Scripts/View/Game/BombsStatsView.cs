﻿using Logic.Game;
using UnityEngine;
using UnityEngine.UI;

namespace View.Game
{
    public class BombsStatsView : MonoBehaviour
    {
        public Text counter;
        public GameModel gameModel;

        private void Awake()
        {
            gameModel.onGameReady.AddListener(UpdateStats);
            gameModel.onWin.AddListener(UpdateStats);
            gameModel.onLose.AddListener(UpdateStats);
            gameModel.onCellSetMark.AddListener(CellSetMarkHandler);
        }

        private void CellSetMarkHandler(int x, int y, bool markSet)
        {
            UpdateStats();
        }

        private void UpdateStats()
        {
            var numBombsLeft = !gameModel.isGameFinished
                ? gameModel.GetNumBombs() - gameModel.GetNumMarkedCells()
                : 0;
            
            counter.text = numBombsLeft.ToString();
        }
    }
}