﻿using Logic.Game;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using View.Game.Events;

namespace View.Game
{
    public class GameFieldPointerHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [HideInInspector] public CellClickEvent onCellClick = new CellClickEvent();
        [HideInInspector] public UnityEvent onEnterBothButtonsMode = new UnityEvent();
        [HideInInspector] public UnityEvent onExitBothButtonsMode = new UnityEvent();

        private bool _rightDown, _leftDown;
        private bool _wasBothClick;
        
        public void OnPointerDown(PointerEventData eventData)
        {
            switch (eventData.button)
            {
                case PointerEventData.InputButton.Left:
                    _leftDown = true;
                    break;
                case PointerEventData.InputButton.Right:
                    _rightDown = true;
                    break;
                default:
                    return;
            }

            if (_leftDown && _rightDown)
            {
                onEnterBothButtonsMode.Invoke();
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ClickType clickType;

            switch (eventData.button)
            {
                case PointerEventData.InputButton.Left:
                    clickType = _rightDown ? ClickType.Both : ClickType.Left;
                    _leftDown = false;
                    break;
                case PointerEventData.InputButton.Right:
                    clickType = _leftDown ? ClickType.Both : ClickType.Right;
                    _rightDown = false;
                    break;
                default:
                    return;
            }

            if (_wasBothClick && clickType != ClickType.Both)
            {
                _wasBothClick = false;
                return;
            }
            
            _wasBothClick = clickType == ClickType.Both;

            if (_wasBothClick)
            {
                onExitBothButtonsMode.Invoke();
            }

            var cellGameObject = FindCellUnderPointer(eventData);
            if (cellGameObject != null)
            {
                var cellView = cellGameObject.GetComponent<CellView>();
                onCellClick.Invoke(cellView.x, cellView.y, clickType);
            }
        }

        // eventData.hovered is empty when right click
        private GameObject FindCellUnderPointer(PointerEventData eventData)
        {
            var obj = eventData.pointerPressRaycast.gameObject;
            while (obj != null && obj.GetComponent<CellView>() == null)
            {
                obj = obj.transform.parent != null ? obj.transform.parent.gameObject : null;
            }

            return obj;
        }

        private void OnDestroy()
        {
            onCellClick.RemoveAllListeners();
            onEnterBothButtonsMode.RemoveAllListeners();
            onExitBothButtonsMode.RemoveAllListeners();
        }
    }
}