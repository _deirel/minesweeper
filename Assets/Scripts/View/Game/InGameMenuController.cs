﻿using System;
using Logic.Game;
using Services;
using UnityEngine;
using View.Game.Windows;

namespace View.Game
{
    public class InGameMenuController : MonoBehaviour
    {
        public GameModel gameModel;
        public WindowsManager windows;
        
        public void SaveClickHandler()
        {
            var save = gameModel.GetSaveData();
            try
            {
                ServiceLocator.instance.saveSystem.SaveData(save);
                windows.saveSuccessPopup.ShowAndHide();
            }
            catch (Exception e)
            {
                windows.saveFailedPopup.ShowAndHide();
                Debug.LogException(e);
            }
        }
        
        public void ExitClickHandler()
        {
            windows.exitWarningWindow.Show();
        }
    }
}