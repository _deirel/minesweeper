﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using View.Game.Events;
using View.Game.Highlight;

namespace View.Game
{
    public class CellView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        public int x, y;

        public GameObject closed;
        public GameObject bomb;
        public GameObject mark;
        public GameObject number;

        [HideInInspector] public CellEvent onPointerEnter = new CellEvent();
        [HideInInspector] public CellEvent onPointerExit = new CellEvent();

        private bool _isOpened;
        private bool _isHighlighted;
        private CellHighlighter _highlighter;

        public void Awake()
        {
            SetAsEmpty();
            SetMark(false);
            _highlighter = GetComponent<CellHighlighter>();
        }

        public void SetAsEmpty()
        {
            bomb.SetActive(false);
            number.SetActive(false);
        }

        public void SetAsHasBombNear(int numBombsNear)
        {
            bomb.SetActive(false);
            number.SetActive(true);
            number.GetComponent<Text>().text = numBombsNear.ToString();
        }

        public void SetAsBomb()
        {
            bomb.SetActive(true);
            number.SetActive(false);
        }

        public void SetMark(bool value)
        {
            if (!_isOpened)
            {
                mark.SetActive(value);
            }
        }

        public void Open(bool animate)
        {
            if (_isOpened)
            {
                return;
            }

            SetMark(false);
            _isOpened = true;

            if (animate)
            {
                StartCoroutine("OpenCoroutine");
            }
            else
            {
                StopCoroutine("OpenCoroutine");
                closed.SetActive(false);
            }
        }

        private IEnumerator OpenCoroutine()
        {
            var image = closed.GetComponent<RawImage>();

            for (var alpha = 1f; alpha >= 0f; alpha -= Time.deltaTime * 4)
            {
                var color = image.color;
                color.a = alpha;
                image.color = color;
                yield return null;
            }

            closed.SetActive(false);
        }

        public void Highlight()
        {
            if (!_isHighlighted)
            {
                _isHighlighted = true;
                _highlighter.Hightlight();
            }
        }

        public void Unhighlight()
        {
            if (_isHighlighted)
            {
                _isHighlighted = false;
                _highlighter.Unhighlight();
            }
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            onPointerEnter.Invoke(new Vector2Int(x, y));
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            onPointerExit.Invoke(new Vector2Int(x, y));
        }

        private void OnDestroy()
        {
            onPointerEnter.RemoveAllListeners();
            onPointerExit.RemoveAllListeners();
        }
    }
}