﻿using Services;
using View.Windows;

namespace View.Game.Windows
{
    public class WindowExitWarningController : WindowController
    {
        public void CancelClickHandler()
        {
            Hide();
        }

        public void ExitClickHandler()
        {
            ServiceLocator.instance.gameManager.ExitGame();
        }
    }
}