﻿using Logic.Game;
using Services;
using UnityEngine;
using View.Windows;

namespace View.Game.Windows
{
    public class WindowLoseController : WindowController
    {
        public GameModel gameModel;
        public GameObject loadGameButton;

        private void Start()
        {
            loadGameButton.SetActive(ServiceLocator.instance.saveSystem.IsSaveExisted());
        }

        public void NewGameClickHandler()
        {
            ServiceLocator.instance.gameManager.NewGame(gameModel.GetMode());
        }

        public void LoadGameClickHandler()
        {
            ServiceLocator.instance.gameManager.LoadGame();
        }

        public void ExitClickHandler()
        {
            ServiceLocator.instance.gameManager.ExitGame();
        }
    }
}