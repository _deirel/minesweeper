﻿using Logic.Game;
using Services;
using View.Windows;

namespace View.Game.Windows
{
    public class WindowWinController : WindowController
    {
        public GameModel gameModel;

        public void NewGameClickHandler()
        {
            ServiceLocator.instance.gameManager.NewGame(gameModel.GetMode());
        }

        public void ExitClickHandler()
        {
            ServiceLocator.instance.gameManager.ExitGame();
        }
    }
}