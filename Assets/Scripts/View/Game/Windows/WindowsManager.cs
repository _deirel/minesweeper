﻿using UnityEngine;
using View.Windows;

namespace View.Game.Windows
{
    public class WindowsManager : MonoBehaviour
    {
        public WindowLoseController loseWindow;
        public WindowExitWarningController exitWarningWindow;
        public WindowWinController winWindow;

        public WindowPopupController saveSuccessPopup;
        public WindowPopupController saveFailedPopup;
    }
}