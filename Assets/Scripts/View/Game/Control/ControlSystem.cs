﻿using Logic.Game;
using UnityEngine;
using UnityEngine.Events;
using View.Game.Events;

namespace View.Game.Control
{
    public class ControlSystem : MonoBehaviour
    {
        public GameView gameView;
        public GameFieldPointerHandler pointerHandler;

        [HideInInspector] public CellClickEvent onCellClick = new CellClickEvent();
        [HideInInspector] public UnityEvent onEnterBothButtonsMode = new UnityEvent();
        [HideInInspector] public UnityEvent onExitBothButtonsMode = new UnityEvent();
        [HideInInspector] public CellEvent onChangeCellUnderPointer = new CellEvent();

        private Vector2Int? _currentCell;

        private void Start()
        {
            AddViewListeners();
            AddPointerListeners();
        }

        private void AddViewListeners()
        {
            gameView.onFieldCreated.AddListener(FieldCreatedHandler);
        }

        private void AddPointerListeners()
        {
            pointerHandler.onCellClick.AddListener(CellClickHandler);
            pointerHandler.onEnterBothButtonsMode.AddListener(EnterBothButtonsModeHandler);
            pointerHandler.onExitBothButtonsMode.AddListener(ExitBothButtonsModeHandler);
        }

        private void FieldCreatedHandler()
        {
            gameView.GetComponent<GameFieldView>().ForEachCell(cellView =>
            {
                cellView.onPointerEnter.AddListener(CellPointerEnterHandler);
                cellView.onPointerExit.AddListener(CellPointerExitHandler);
            });
        }

        private void CellPointerEnterHandler(Vector2Int? cellPosition)
        {
            if (!ComparePositions(cellPosition, _currentCell))
            {
                onChangeCellUnderPointer.Invoke(_currentCell = cellPosition);
            }
        }

        private void CellPointerExitHandler(Vector2Int? cellPosition)
        {
            if (ComparePositions(cellPosition, _currentCell))
            {
                onChangeCellUnderPointer.Invoke(_currentCell = null);
            }
        }

        private bool ComparePositions(Vector2Int? pos1, Vector2Int? pos2)
        {
            if (pos1 == null && pos2 != null || pos1 != null && pos2 == null)
            {
                return false;
            }

            if (pos1 == null)
            {
                return true;
            }

            return pos1.Value.x == pos2.Value.x && pos1.Value.y == pos2.Value.y;
        }

        private void CellClickHandler(int x, int y, ClickType clickType)
        {
            onCellClick.Invoke(x, y, clickType);
        }

        private void EnterBothButtonsModeHandler()
        {
            onEnterBothButtonsMode.Invoke();
            Debug.Log("Enter both buttons mode");
        }

        private void ExitBothButtonsModeHandler()
        {
            onExitBothButtonsMode.Invoke();
            Debug.Log("Exit both buttons mode");
        }

        private void OnDestroy()
        {
            onCellClick.RemoveAllListeners();
            onEnterBothButtonsMode.RemoveAllListeners();
            onExitBothButtonsMode.RemoveAllListeners();
            onChangeCellUnderPointer.RemoveAllListeners();
        }
    }
}