﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace View.Game.AutoSize
{
    [RequireComponent(typeof(GameView))]
    public class GameFieldFitter : UIBehaviour
    {
        public bool isReady { get; private set; }
        
        private GameView _view;

        protected override void Start()
        {
            base.Start();
            _view = GetComponent<GameView>();
            _view.onFieldCreated.AddListener(UpdateScale);
        }

        public void UpdateScale()
        {
            var fieldRect = GetComponent<RectTransform>().rect;
            var parentRect = transform.parent.GetComponent<RectTransform>().rect;
            
            var fieldAspectRatio = fieldRect.width / fieldRect.height;
            var parentAspectRatio = parentRect.width / parentRect.height;
            
            var scale = fieldAspectRatio > parentAspectRatio
                ? parentRect.width / fieldRect.width
                : parentRect.height / fieldRect.height;
            
            transform.localScale = new Vector3(scale, scale, scale);

            isReady = true;
        }
    }
}