﻿using UnityEngine.EventSystems;

namespace View.Game.AutoSize
{
    public class GameFieldResizeByBounds : UIBehaviour
    {
        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
            var fitter = GetComponentInChildren<GameFieldFitter>();
            if (fitter != null && fitter.isReady)
            {
                fitter.UpdateScale();
            }
        }
    }
}