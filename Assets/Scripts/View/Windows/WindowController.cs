﻿using System.Collections;
using UnityEngine;

namespace View.Windows
{
    public class WindowController : MonoBehaviour
    {
        private Coroutine _lastCoroutine;
        
        public float windowAlpha = 1f;
        public float fadeSpeed = 0.2f;
        
        public void Show()
        {
            if (!gameObject.activeSelf)
            {
                group.alpha = 0f;
                gameObject.SetActive(true);
            }

            StartWindowAnimationCoroutine(ShowCoroutine());
        }

        public void Hide()
        {
            StartWindowAnimationCoroutine(HideCoroutine());
        }

        protected IEnumerator ShowCoroutine()
        {
            for (var a = group.alpha; a < windowAlpha; a += Time.deltaTime / fadeSpeed)
            {
                group.alpha = a;
                yield return null;
            }

            group.alpha = windowAlpha;
        }

        protected IEnumerator HideCoroutine()
        {
            for (var a = group.alpha; a > 0f; a -= Time.deltaTime / fadeSpeed)
            {
                group.alpha = a;
                yield return null;
            }

            group.alpha = 0f;
            gameObject.SetActive(false);
        }

        protected void CancelWindowAnimationCoroutine()
        {
            if (_lastCoroutine != null)
            {
                StopCoroutine(_lastCoroutine);
            }
        }

        protected void StartWindowAnimationCoroutine(IEnumerator routine)
        {
            CancelWindowAnimationCoroutine();
            _lastCoroutine = StartCoroutine(routine);
        }

        private CanvasGroup _group;

        protected CanvasGroup group
        {
            get { return _group ?? (_group = GetComponent<CanvasGroup>()); }
        }
    }
}