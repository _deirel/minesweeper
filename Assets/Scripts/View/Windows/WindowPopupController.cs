﻿using System.Collections;
using UnityEngine;

namespace View.Windows
{
    public class WindowPopupController : WindowController
    {
        public float stayTime = 1f;
        
        public void ShowAndHide()
        {
            if (!gameObject.activeSelf)
            {
                group.alpha = 0f;
                gameObject.SetActive(true);
            }
            
            StartWindowAnimationCoroutine(ShowAndHideCoroutine());
        }

        private IEnumerator ShowAndHideCoroutine()
        {
            yield return ShowCoroutine();
            yield return new WaitForSeconds(stayTime);
            yield return HideCoroutine();
        }
    }
}