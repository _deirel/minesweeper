﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace View.Loading
{
    public class LoadingController : MonoBehaviour
    {
        private void Start()
        {
            // Loading stuff
            SceneManager.LoadScene("Menu");
        }
    }
}