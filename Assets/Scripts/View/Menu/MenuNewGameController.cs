﻿using Logic.Game;
using Services;
using UnityEngine;

namespace View.Menu
{
    public class MenuNewGameController : MonoBehaviour
    {
        public MenuManager menuManager;

        public void NoviceClickHandler()
        {
            ServiceLocator.instance.gameManager.NewGame(GameMode.Novice);
        }

        public void IntermediateClickHandler()
        {
            ServiceLocator.instance.gameManager.NewGame(GameMode.Intermediate);
        }

        public void ProfessionalClickHandler()
        {
            ServiceLocator.instance.gameManager.NewGame(GameMode.Professional);
        }

        public void BackClickHandler()
        {
            menuManager.SwitchTo(menuManager.menuMain);
        }
    }
}

