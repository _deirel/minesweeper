﻿using UnityEngine;

namespace View.Menu
{
    public class MenuManager : MonoBehaviour
    {
        public GameObject menuMain;
        public GameObject menuNewGame;

        private GameObject[] _menus;

        public void Start()
        {
            _menus = new[] {menuMain, menuNewGame};
        }

        public void SwitchTo(GameObject targetMenu)
        {
            foreach (var menu in _menus)
            {
                menu.SetActive(menu == targetMenu);
            }
        }
    }
}