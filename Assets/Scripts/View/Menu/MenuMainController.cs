﻿using System;
using Services;
using UnityEngine;
using View.Windows;

namespace View.Menu
{
    public class MenuMainController : MonoBehaviour
    {
        public MenuManager menuManager;
        public GameObject loadGameButton;
        public WindowPopupController loadingFailedPopup;
        
        public void Start()
        {
            var isSaveExisted = ServiceLocator.instance.saveSystem.IsSaveExisted();
            loadGameButton.SetActive(isSaveExisted);
        }

        public void NewGameClickHandler()
        {
            menuManager.SwitchTo(menuManager.menuNewGame);
        }

        public void LoadGameClickHandler()
        {
            try
            {
                ServiceLocator.instance.gameManager.LoadGame();
            }
            catch (Exception)
            {
                loadingFailedPopup.ShowAndHide();
            }
        }
    }
}

