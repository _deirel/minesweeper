﻿using System;

namespace Utils
{
    public class CoordsUtil
    {
        public static bool IsInRange(int x, int y, int width, int height)
        {
            return x > -1 && y > -1 && x < width && y < height;
        }

        public static void InvokeForNearestPositions(int x, int y, int width, int height, Action<int, int> callback)
        {
            InvokeForPosition(x - 1, y - 1, width, height, callback);
            InvokeForPosition(x,     y - 1, width, height, callback);
            InvokeForPosition(x + 1, y - 1, width, height, callback);
            InvokeForPosition(x + 1, y    , width, height, callback);
            InvokeForPosition(x + 1, y + 1, width, height, callback);
            InvokeForPosition(x,     y + 1, width, height, callback);
            InvokeForPosition(x - 1, y + 1, width, height, callback);
            InvokeForPosition(x - 1, y    , width, height, callback);
        }

        private static void InvokeForPosition(int x, int y, int width, int height, Action<int, int> callback)
        {
            if (IsInRange(x, y, width, height))
            {
                callback(x, y);
            }
        }
    }
}