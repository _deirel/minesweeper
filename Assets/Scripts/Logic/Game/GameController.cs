﻿using Services;
using UnityEngine;
using View.Game;
using View.Game.Control;
using View.Game.Windows;

namespace Logic.Game
{
    public class GameController : MonoBehaviour
    {
        public GameModel model;
        public GameView view;
        public ControlSystem controls;
        public WindowsManager windows;

        public void Start()
        {
            AddModelListeners();
            AddControlsListeners();
            StartGame();
        }

        private void AddModelListeners()
        {
            model.onGameReady.AddListener(GameReadyHandler);
            model.onLose.AddListener(LoseHandler);
            model.onWin.AddListener(WinHandler);
        }

        private void AddControlsListeners()
        {
            controls.onCellClick.AddListener(CellClickHandler);
        }

        private void StartGame()
        {
            var modelInitializer = ServiceLocator.instance.gameManager.GetGameModelInitializer();
            modelInitializer.InitModel(model);
        }

        private void GameReadyHandler()
        {
            view.CreateField();
        }

        private void CellClickHandler(int x, int y, ClickType clickType)
        {
            if (model.isGameFinished)
            {
                return;
            }

            switch (clickType)
            {
                case ClickType.Left:
                    model.OpenCell(x, y);
                    break;
                case ClickType.Right:
                    model.SwitchMark(x, y);
                    break;
                case ClickType.Both:
                    model.OpenNearestUnmarkedCells(x, y);
                    break;
            }
        }

        private void LoseHandler()
        {
            windows.loseWindow.Show();
        }

        private void WinHandler()
        {
            windows.winWindow.Show();
        }
    }
}