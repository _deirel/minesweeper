﻿namespace Logic.Game
{
    public interface IGameModelInitializer
    {
        void Prepare();
        void InitModel(GameModel model);
    }
}