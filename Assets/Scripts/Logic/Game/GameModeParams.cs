﻿using System;
using System.Collections.Generic;

namespace Logic.Game
{
    public class GameModeParams
    {
        private static Dictionary<GameMode, GameModeParams> _byMode = new Dictionary<GameMode, GameModeParams>
        {
            {GameMode.Novice, new GameModeParams(10, 10, 20)},
            {GameMode.Intermediate, new GameModeParams(20, 20, 40)},
            {GameMode.Professional, new GameModeParams(30, 30, 90)}
        };

        public static GameModeParams getByMode(GameMode mode)
        {
            GameModeParams gameParams;
            if (!_byMode.TryGetValue(mode, out gameParams))
            {
                throw new Exception("Unknown game start params for given mode");
            }

            return gameParams;
        }

        public int width, height;
        public int numBombs;

        private GameModeParams(int width, int height, int numBombs)
        {
            this.width = width;
            this.height = height;
            this.numBombs = numBombs;
        }
    }
}