﻿namespace Logic.Game
{
    public enum GameMode
    {
        Novice, Intermediate, Professional
    }
}
