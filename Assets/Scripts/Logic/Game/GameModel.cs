﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logic.Game.Events;
using Logic.Save;
using UnityEngine;
using UnityEngine.Events;
using Utils;
using Random = System.Random;

namespace Logic.Game
{
    public class GameModel : MonoBehaviour
    {
        [HideInInspector] public UnityEvent onGameReady = new UnityEvent();
        [HideInInspector] public UnityEvent onCellsGenerated = new UnityEvent();
        [HideInInspector] public CellOpenEvent onCellOpen = new CellOpenEvent();
        [HideInInspector] public CellSetMarkEvent onCellSetMark = new CellSetMarkEvent();
        [HideInInspector] public UnityEvent onLose = new UnityEvent();
        [HideInInspector] public UnityEvent onWin = new UnityEvent();

        [HideInInspector] public int width, height;
        public bool isGameFinished { get; private set; }

        private Cell[] _cells;
        private int _numCells;
        private int _numBombs;
        private GameMode _mode;

        private bool _deferredGeneration;
        private int _numBombsToGenerate;

        public void InitByMode(GameMode mode)
        {
            _mode = mode;

            var gameModeParams = GameModeParams.getByMode(mode);
            Init(gameModeParams.width, gameModeParams.height);

            _deferredGeneration = true;
            _numBombsToGenerate = gameModeParams.numBombs;

            onGameReady.Invoke();
        }

        public void InitFromSave(SaveData save)
        {
            _mode = save.mode;

            Init(save.width, save.height);
            InitCellsBySave(save);

            if (save.deferredGeneration)
            {
                _deferredGeneration = true;
                _numBombsToGenerate = save.numBombs;
            }

            onGameReady.Invoke();

            if (!_deferredGeneration)
            {
                onCellsGenerated.Invoke();
            }
        }

        private void Init(int width, int height)
        {
            this.width = width;
            this.height = height;
            _numCells = width * height;
            CreateCells();
        }

        private void CreateCells()
        {
            _cells = new Cell[_numCells];
            var id = 0;
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    _cells[id++] = new Cell(i, j);
                }
            }
        }

        private void InitCellsBySave(SaveData save)
        {
            foreach (var bombPos in save.bombs)
            {
                GetCell(bombPos.x, bombPos.y).isBomb = true;
            }

            foreach (var markPos in save.marks)
            {
                GetCell(markPos.x, markPos.y).isMarked = true;
            }

            foreach (var openedPos in save.opened)
            {
                GetCell(openedPos.x, openedPos.y).isOpened = true;
            }

            UpdateNumBombs();
            CalculateNeighbours();
        }

        public void OpenCell(int x, int y)
        {
            CheckDeferredGeneration(x, y);

            var cell = GetCell(x, y);
            if (cell.isOpened)
            {
                return;
            }

            if (cell.isBomb)
            {
                cell.isOpened = true;
                onCellOpen.Invoke(cell.x, cell.y);

                OnLose();
            }
            else
            {
                var openedCells = CalculateCellsOpenArea(cell);
                foreach (var cellToOpen in openedCells)
                {
                    if (cellToOpen.isMarked)
                    {
                        SwitchMark(cellToOpen.x, cellToOpen.y);
                    }

                    cellToOpen.isOpened = true;
                    onCellOpen.Invoke(cellToOpen.x, cellToOpen.y);
                }

                CheckWinCondition();
            }
        }

        private List<Cell> CalculateCellsOpenArea(Cell startCell)
        {
            var area = new List<Cell>();
            CalculateCellsOpenAreaRecursively(startCell, area, new Dictionary<Cell, bool>());
            return area;
        }

        private void CalculateCellsOpenAreaRecursively(Cell cell, List<Cell> area, Dictionary<Cell, bool> passedCells)
        {
            if (passedCells.ContainsKey(cell)) return;

            passedCells[cell] = true;

            if (cell.isBomb || cell.isOpened) return;

            area.Add(cell);
            if (cell.numBombsNear == 0)
            {
                ForEachNearestCell(
                    cell.x,
                    cell.y,
                    nearestCell => CalculateCellsOpenAreaRecursively(nearestCell, area, passedCells)
                );
            }
        }

        private void CheckWinCondition()
        {
            var isWin = _cells.Count(c => c.isBomb && !c.isOpened ||
                                          !c.isBomb && c.isOpened) == width * height;
            if (isWin)
            {
                OnWin();
            }
        }

        private void CheckDeferredGeneration(int startCellX, int startCellY)
        {
            if (_deferredGeneration)
            {
                _deferredGeneration = false;
                GenerateBombs(_numBombsToGenerate, startCellX, startCellY);
                onCellsGenerated.Invoke();
            }
        }

        public void OpenNearestUnmarkedCells(int x, int y)
        {
            if (GetCell(x, y).isOpened)
            {
                ForEachNearestCell(x, y, cell =>
                {
                    if (!cell.isMarked)
                    {
                        OpenCell(cell.x, cell.y);
                    }
                });
            }
        }

        private void GenerateBombs(int numBombs, int startCellX, int startCellY)
        {
            var cellsList = _cells.ToList();
            var numCellsRemains = _numCells;

            var startCellId = GetCellIdByPosition(startCellX, startCellY);
            cellsList[startCellId] = cellsList[numCellsRemains - 1];
            cellsList.RemoveAt(--numCellsRemains);

            var random = new Random();
            for (var i = 0; i < numBombs && numCellsRemains > 0; i++)
            {
                var randomCellId = random.Next(numCellsRemains);
                var randomCell = cellsList[randomCellId];

                randomCell.isBomb = true; 

                cellsList[randomCellId] = cellsList[numCellsRemains - 1];
                cellsList.RemoveAt(--numCellsRemains);
            }

            UpdateNumBombs();
            CalculateNeighbours();
        }

        private void UpdateNumBombs()
        {
            _numBombs = _cells.Count(cell => cell.isBomb);
        }

        private void CalculateNeighbours()
        {
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    GetCellUnsafe(i, j).numBombsNear = GetNumBombsNear(i, j);
                }
            }
        }

        private int GetNumBombsNear(int x, int y)
        {
            var numBombsNear = 0;
            ForEachNearestCell(x, y, cell => numBombsNear += cell.isBomb ? 1 : 0);
            return numBombsNear;
        }

        public void SwitchMark(int x, int y)
        {
            var cell = GetCell(x, y);
            if (cell.isOpened)
            {
                return;
            }

            cell.isMarked = !cell.isMarked;
            onCellSetMark.Invoke(x, y, cell.isMarked);
        }

        private void OnLose()
        {
            isGameFinished = true;
            InvokeOpenAllCells();
            onLose.Invoke();
        }

        private void OnWin()
        {
            isGameFinished = true;
            InvokeOpenAllCells();
            onWin.Invoke();
        }

        private void InvokeOpenAllCells()
        {
            foreach (var cell in _cells)
            {
                if (!cell.isOpened)
                {
                    onCellOpen.Invoke(cell.x, cell.y);
                }
            }
        }

        private void ForEachNearestCell(int x, int y, Action<Cell> callback)
        {
            CoordsUtil.InvokeForNearestPositions(x, y, width, height, (cellX, cellY) =>
                {
                    callback(GetCellUnsafe(cellX, cellY));
                });
        }

        public Cell GetCell(int x, int y)
        {
            if (!CoordsUtil.IsInRange(x, y, width, height))
            {
                throw new Exception("Can't access the cell at {" + x + ", " + y + "}");
            }

            return _cells[GetCellIdByPosition(x, y)];
        }

        private Cell GetCellUnsafe(int x, int y)
        {
            return _cells[GetCellIdByPosition(x, y)];
        }

        private int GetCellIdByPosition(int x, int y)
        {
            return x * height + y;
        }

        public int GetNumMarkedCells()
        {
            return _cells.Count(cell => cell.isMarked);
        }

        public int GetNumBombs()
        {
            return _deferredGeneration ? _numBombsToGenerate : _numBombs;
        }

        public GameMode GetMode()
        {
            return _mode;
        }

        public SaveData GetSaveData()
        {
            var save = new SaveData
            {
                width = width,
                height = height,
                mode = _mode
            };

            if (_deferredGeneration)
            {
                save.deferredGeneration = true;
                save.numBombs = _numBombsToGenerate;
            }

            foreach (var cell in _cells)
            {
                if (cell.isBomb)
                {
                    save.bombs.Add(new Position(cell.x, cell.y));
                }

                if (cell.isMarked)
                {
                    save.marks.Add(new Position(cell.x, cell.y));
                }

                if (cell.isOpened)
                {
                    save.opened.Add(new Position(cell.x, cell.y));
                }
            }

            return save;
        }

        private void OnDestroy()
        {
            onGameReady.RemoveAllListeners();
            onCellsGenerated.RemoveAllListeners();
            onCellOpen.RemoveAllListeners();
            onCellSetMark.RemoveAllListeners();
            onLose.RemoveAllListeners();
            onWin.RemoveAllListeners();
        }
    }
}