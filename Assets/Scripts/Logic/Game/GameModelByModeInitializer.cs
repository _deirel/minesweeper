﻿namespace Logic.Game
{
    public class GameModelByModeInitializer : IGameModelInitializer
    {
        private readonly GameMode _mode;
        
        public GameModelByModeInitializer(GameMode mode)
        {
            _mode = mode;
        }

        public void Prepare()
        {
        }

        public void InitModel(GameModel model)
        {
            model.InitByMode(_mode);
        }
    }
}