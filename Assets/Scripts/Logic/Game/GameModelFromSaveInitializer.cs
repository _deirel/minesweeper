﻿using System;
using Logic.Save;
using Services;

namespace Logic.Game
{
    public class GameModelFromSaveInitializer : IGameModelInitializer
    {
        private SaveData _save;
        
        public void Prepare()
        {
            _save = ServiceLocator.instance.saveSystem.LoadData();
        }

        public void InitModel(GameModel model)
        {
            if (_save == null)
            {
                throw new Exception("The model initializer is not prepared");
            }
            
            model.InitFromSave(_save);
        }
    }
}