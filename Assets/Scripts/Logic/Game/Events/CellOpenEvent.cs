﻿using UnityEngine.Events;

namespace Logic.Game.Events
{
    public class CellOpenEvent : UnityEvent<int, int>
    {
    }
}