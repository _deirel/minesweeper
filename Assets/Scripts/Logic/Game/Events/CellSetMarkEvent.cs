﻿using UnityEngine.Events;

namespace Logic.Game.Events
{
    public class CellSetMarkEvent : UnityEvent<int, int, bool>
    {
    }
}