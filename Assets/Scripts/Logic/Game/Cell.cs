﻿namespace Logic.Game
{
    public class Cell
    {
        public readonly int x, y;
        
        public bool isBomb = false;
        public bool isMarked = false;
        public bool isOpened = false;
        
        public int numBombsNear;
        
        public Cell(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}