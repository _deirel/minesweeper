﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logic.Game;

namespace Logic.Save
{
    [Serializable]
    public class SaveData
    {
        public int width;
        public int height;
        public GameMode mode;
        public List<Position> bombs = new List<Position>();
        public List<Position> marks = new List<Position>();
        public List<Position> opened =new List<Position>();
        public bool deferredGeneration;
        public int numBombs;

        public bool IsValid()
        {
            return width > 0
                   && height > 0
                   && IsPositionsValid(bombs)
                   && IsPositionsValid(marks)
                   && IsPositionsValid(opened);
        }

        private bool IsPositionsValid(List<Position> positions)
        {
            return positions != null && positions.All(p => p.x >= 0 && p.y >= 0 && p.x < width && p.y < height);
        }
    }

    [Serializable]
    public struct Position
    {
        public int x, y;

        public Position(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}